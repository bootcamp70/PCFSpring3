package com.examples.spring.springweb;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/helloworld")
public class MyController {
  
  @GetMapping()
  public String get() {
    return "first api to deploy in spring";
  }

  
}